import cv2
import numpy as np
import argparse

# handle args
parser = argparse.ArgumentParser(description="Add hat on a head in a video stream")
parser.add_argument(
    "--cascade-classifier",
    "-c",
    default="data/head.xml",
    help="a path to an openCV cascade classifier",
)
parser.add_argument(
    "--hat",
    "-H",
    default="data/hat.png",
    help="path to a hat picture (a transparency channel is required)",
)
parser.add_argument("--video", "-v", type=int, default=0, help="camera index")
parser.add_argument(
    "--momentum",
    "-m",
    type=float,
    default=0.6,
    help="momentum of the filter (between 0 and 1)",
)
args = parser.parse_args()
assert 0.0 <= args.momentum < 1.0

# open cascade filter
faceCascade = cv2.CascadeClassifier(args.cascade_classifier)

# open video
video_capture = cv2.VideoCapture(args.video)

# load the hat
img = cv2.imread(args.hat, flags=cv2.IMREAD_UNCHANGED)
h, w, _ = img.shape

# setup the filter
f_x, f_y, f_w, f_h = None, None, None, None
momentum = args.momentum

while True:
    ret, frame = video_capture.read()

    # head detection
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE,
    )

    # get the first face
    for (c_x, c_y, c_w, c_h) in faces:
        if f_x is None:
            f_x, f_y, f_w, f_h = c_x, c_y, c_w, c_h
        else:
            # Exponential Moving Average for stability
            f_x = (c_x * (1.0 - momentum) + f_x * momentum).astype(int)
            f_y = (c_y * (1.0 - momentum) + f_y * momentum).astype(int)
            f_w = (c_w * (1.0 - momentum) + f_w * momentum).astype(int)
            f_h = (c_h * (1.0 - momentum) + f_h * momentum).astype(int)

        # reshape the hat picture
        reshaped = cv2.resize(img, (0, 0), fx=f_w / w, fy=f_w / w)
        r_h, r_w, _ = reshaped.shape
        # generate a mask to insert pixels without transparency
        mask = np.repeat((reshaped[:, :, -1:] > 127).astype(np.uint8), 3, axis=2)

        # claculate the border
        b_y1, b_y2, b_x1, b_x2 = f_y - f_h, f_y - f_h + r_h, f_x, f_x + r_w
        f_y1, f_y2, f_x1, f_x2 = 0, r_h, 0, r_w

        # crop image if needed
        if b_y1 < 0:
            f_y1 = -b_y1
            b_y1 = 0

        try:
            # insert the hat
            frame[b_y1:b_y2, b_x1:b_x2] = (1.0 - mask[f_y1:f_y2, f_x1:f_x2]) * frame[
                b_y1:b_y2, b_x1:b_x2
            ] + mask[f_y1:f_y2, f_x1:f_x2] * reshaped[f_y1:f_y2, f_x1:f_x2, :-1]
        except ValueError:
            pass

        # cv2.rectangle(frame, (f_x, f_y), (f_x + f_w, f_y + f_h), (0, 255, 0), 2)
        break

    # display
    cv2.imshow("Video", frame)

    # exit if needed
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# release
video_capture.release()
cv2.destroyAllWindows()
